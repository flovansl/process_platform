use std::process::Command;

slint::include_modules!();

use std::io::Read;
use std::vec;

fn main() {
    let ui = AppWindow::new();

    let width = 200;
    let height = 200;

    // let dummy = vec![0; width as usize * height as usize];

    // // result.read_to_string(buf)
    // let mut buffer = slint::SharedPixelBuffer::<slint::Rgba8Pixel>::new(width, height);

    // let output = command.stdout;

    let mut command = Command::new(
        "/Users/flovansl/Projects/flovansl/process_platform/target/debug/client",
    )
    .stdout(std::process::Stdio::piped())
    .spawn()
    .unwrap();

    let handle = std::thread::spawn({
        let ui = ui.as_weak();
        move || {
          

            let mut buf = vec![];
            command.stdout.as_mut().unwrap().read_to_end(&mut buf).unwrap();

            ui.upgrade_in_event_loop(move |h| {
                h.set_image(slint::Image::from_rgba8(
                    slint::SharedPixelBuffer::clone_from_slice(&buf, width, height),
                ));
            });

            loop {}
        }
    });

    // let mut test = vec![0; width as usize * height as usize * 4];
    // let result = command.stdout.take().unwrap().read(&mut buffer.make_mut_bytes());
    // let len = buffer.make_mut_bytes().len();

    // ui.set_image(slint::Image::from_rgba8(slint::SharedPixelBuffer::clone_from_slice(&buf, width, height)));

    ui.run();

    // command.kill().unwrap();
}
