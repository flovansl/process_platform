slint::include_modules!();

use std::io::{self, BufWriter, Write};

use std::io::stdout;

use std::process::{Command, ExitStatus, Stdio};

fn main() {
    let ui = AppWindow::new();

    let width = 200;
    let height = 200;

    // let color = slint::Color::from_rgb_u8(0, 255, 0);

    std::thread::spawn(move || {
        let out_buffer = vec![
            slint::Rgba8Pixel {
                r: 144,
                g: 100,
                b: 200,
                a: 255
            };
            width as usize * height as usize
        ];

        let buffer_slice = unsafe {
            std::slice::from_raw_parts(
                out_buffer.as_ptr() as *const u8,
                out_buffer.len() * std::mem::size_of::<slint::Rgba8Pixel>(),
            ) as &[u8]
        };

        let mut bw = BufWriter::new(std::io::stdout().lock());
        bw.write_all(buffer_slice);

        // let mut out =;
        // {

        //     out.write_all(buffer_slice).unwrap();

        // }

        bw.flush();
    });
    ui.run();
}
